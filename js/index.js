$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });
    //el id apunta al id del modal.
    $('#reservar').on('show.bs.modal', function (e){
        console.log('el modal se está mostrando');

        $('#reservaBtn').removeClass('btn-primary');
        $('#reservaBtn').addClass('btn-outline-primary');
        $('#reservaBtn').prop('disabled', true);
        
    });

    $('#reservar').on('shown.bs.modal',function(e){
        console.log('el modal se mostró');
    })
    
    $('#reservar').on('hidde.bs.modal',function(e){
        console.log('el modal se está ocultando');
    })

    $('#reservar').on('hidden.bs.modal', function(e){
        console.log('el modal se ocultó');
        $('#reservaBtn').removeClass('btn-outline-primary');
        $('#reservaBtn').addClass('btn-primary');
        $('#reservaBtn').prop('disabled', false);
    })
});